#include <QDir>
#include <QFileInfo>
#include <QString>
#include <QStringList>

#include "include/ct_filename.h"

bool QCTFilename::isValid()
{
    return _valid;
}

QCTFilename QCTFilename::fromFilename(QString filename)
{
    QCTFilename result;

    QStringList list = QFileInfo(filename).fileName().split(".");
    if (list.count() == 6)
    {
        result.dir = QFileInfo(filename).dir().absolutePath();
        result.name = list[0];
        result.xxx = list[1];
        result.nnn = list[2];
        result._valid = true;
    }
    return result;
}

QString QCTFilename::getFilename()
{
    // QDir to use the correct separator
    return QDir(dir).filePath(QString("%1.%2.%3.%4").arg(name).arg(xxx).arg(nnn).arg(tail));
}
