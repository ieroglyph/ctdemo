#include "include/fps_framesdata.h"
#include "ct_file_processor.h"
#include "ct_filename.h"
#include <iostream>
#include <QCoreApplication>
#include <QDir>
#include <QDirIterator>
#include <QThread>

ct::QFpsFramesdata::QFpsFramesdata(ct::QCTParams params, QObject* parent) :
    QObject(parent), _params(params)
{
}

QStringList ct::QFpsFramesdata::doWork()
{
    QDir dir(_params.dir);

    if (!dir.exists())
    {
        std::cout << "Directory " << dir.absolutePath().toStdString() << " does not exist!"
                  << std::endl;
        qApp->exit(1);
    }

    QDirIterator it(dir.absolutePath(), QStringList() << "*.info.yml.gz", QDir::Files);

    if (!it.hasNext())
    {
        std::cout << "Directory " << dir.absolutePath().toStdString() << " is empty!" << std::endl;
        qApp->exit(2);
    }

    QStringList files;

    while (it.hasNext())
    {
        auto ctfn = QCTFilename::fromFilename(it.next());
        if (!ctfn.isValid())
            continue;
        bool ok = false;
        int nnn = ctfn.nnn.toInt(&ok, 10);
        if (!ok)
            continue;
        if ((_params.start < 0 || nnn >= _params.start) && (_params.end < 0 || nnn <= _params.end))
            files << ctfn.getFilename();
    }

    files.sort();

    return files;
}
