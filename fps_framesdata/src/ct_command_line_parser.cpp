#include "ct_command_line_parser.h"

namespace
{
const char* domain = "main";
const char* option_dir_s = "d";
const char* option_dir_l = "dir";
const char* option_start_s = "s";
const char* option_start_l = "start";
const char* option_end_s = "e";
const char* option_end_l = "end";
const char* option_output_s = "o";
const char* option_output_l = "output";
const char* option_verbose_s = "v";
const char* option_verbose_l = "verbose";
} // namespace

namespace ct
{
QCTCommandLineParser::QCTCommandLineParser()
{
    setApplicationDescription("Test task for Cognitive Technologies.\n"
                              "(c) Anton Kurilenko, 2020");
    addHelpOption();
    addOptions({
        { { ::option_dir_s, ::option_dir_l },
          QCoreApplication::translate(
              ::domain, "Path to a directory containing *.info.yaml.gz files to work with"),
          QCoreApplication::translate(::domain, "path") },
        { { ::option_start_s, ::option_start_l },
          QCoreApplication::translate(
              ::domain, "Episode num to start analysis. If not specified, first file in the dir "
                        "will be used. Must be a number between 0 and 999"),
          QCoreApplication::translate(::domain, "num") },
        { { ::option_end_s, ::option_end_l },
          QCoreApplication::translate(::domain,
                                      "Episode num to end analysis. If no specified, last file in "
                                      "the dir will be userd. Must be a number between 0 and 999"),
          QCoreApplication::translate(::domain, "num") },
        { { ::option_output_s, ::option_output_l },
          QCoreApplication::translate(::domain, "Path to a file to store results in a CSV table. "
                                                "If no specified, stdout will be used."),
          QCoreApplication::translate(::domain, "path") },
    });
}

QCTParams QCTCommandLineParser::process(const QCoreApplication& app)
{
    QCTParams params;
    QCommandLineParser::process(app);

    // if dir is not set then show help and exit
    if (isSet(::option_dir_l) && !value(::option_dir_l).isEmpty())
        params.dir = value(::option_dir_l);
    else
        this->showHelp(1);

    if (isSet(::option_start_l))
    {
        QString start = value(::option_start_l);
        bool ok = false;
        int s = start.toInt(&ok, 10);
        if (!ok)
            this->showHelp(1);
        params.start = s;
    }

    if (isSet(::option_end_l))
    {
        QString end = value(::option_end_l);
        bool ok = false;
        int e = end.toInt(&ok, 10);
        if (!ok)
            this->showHelp(1);
        params.end = e;
    }

    if (isSet(::option_output_l))
        params.output = value(::option_output_l);

    return params;
}
} // namespace ct
