#include <iostream>
#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QMetaObject>
#include <QStringList>

#include "ct_command_line_parser.h"
#include "ct_file_processor.h"
#include "ct_filename.h"
#include "ct_params.h"
#include "fps_framesdata.h"
#include <QThread>

using namespace ct;

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

    // get params or exit
    QCTCommandLineParser parser;
    QCTParams params = parser.process(a);

    QFpsFramesdata fps(params);

    CTFileProcessor fileproc;

    QObject::connect(&fileproc, &CTFileProcessor::filesScanned, [&](QString csvStr) {
        if (params.output == QString())
            std::cout << qPrintable(csvStr);
        else
        {
            QFile outFile(params.output);
            if (!outFile.open(QIODevice::WriteOnly))
            {
                a.exit(11);
            }

            QTextStream outputStream(&outFile);
            outputStream << csvStr;
            outFile.close();
        }
        qApp->quit();
    });

    fileproc.scanFiles(fps.doWork());
    return a.exec();
}
