#ifndef CT_COMMAND_LINE_PARSER_H
#define CT_COMMAND_LINE_PARSER_H

#include "ct_params.h"
#include <QCommandLineParser>

namespace ct
{
/*!
 * \brief The QCTCommandLineParser class
 * Precesses and checks command line arguments. If correct, prepares QCTParams object;
 */
class QCTCommandLineParser : public QCommandLineParser
{
public:
    QCTCommandLineParser();

    /*!
     * \brief process
     * Processes command line arguments.
     * \param app
     * \return A QCTParams object containing parsed values.
     */
    QCTParams process(const QCoreApplication& app);
};

} // namespace ct
#endif // CT_COMMAND_LINE_PARSER_H
