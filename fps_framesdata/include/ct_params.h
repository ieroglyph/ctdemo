#ifndef CT_PARAMS_H
#define CT_PARAMS_H

#include <QString>

namespace ct
{
class QCTParams
{
public:
    QString dir;
    qint32 start{ -1 };
    qint32 end{ -1 };
    QString output;
};

} // namespace ct
#endif // CT_PARAMS_H
