#ifndef FPS_FRAMESDATA_H
#define FPS_FRAMESDATA_H

#include "ct_params.h"
#include <QObject>
#include <QStringList>

namespace ct
{
class QFpsFramesdata : public QObject
{
    Q_OBJECT
public:
    explicit QFpsFramesdata(QCTParams params, QObject* parent = nullptr);

signals:
    void workIsDone();

public slots:
    QStringList doWork();

private:
    QCTParams _params;
};

} // namespace ct
#endif // FPS_FRAMESDATA_H
