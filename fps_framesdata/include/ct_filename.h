#ifndef CT_FILENAME_H
#define CT_FILENAME_H

#include <QString>

class QCTFilename
{
    bool _valid{ false };
    QCTFilename() = default;

public:
    QCTFilename(const QCTFilename&) = default;
    ~QCTFilename() = default;

    QString dir;
    QString name;
    QString xxx;
    QString nnn;
    const QString tail{ "info.yml.gz" };
    bool isValid();
    static QCTFilename fromFilename(QString filename);
    QString getFilename();
};

#endif // CT_FILENAME_H
