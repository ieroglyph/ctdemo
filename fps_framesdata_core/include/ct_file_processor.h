#ifndef CT_FILE_PROCESSOR_H
#define CT_FILE_PROCESSOR_H

#include "export.h"
#include <QMap>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QStringList>

namespace ct
{
class FPS_FRAMESDATA_CORE_EXPORT CTFileProcessor : public QObject
{
    Q_OBJECT

public:
    enum class FileScanResult : int8_t
    {
        Ok = 0,
        ErrorGz,
        ErrorFs,
        ErrorYaml,
    };

    using DeviceMap = QMap<QString, int>;

    CTFileProcessor(QObject* parent = nullptr);
    ~CTFileProcessor();

    /*!
   * \brief scanFile
   * For each file attempts to decompress file, gets it's length and sends it's CTFileInfo.
   * Emits the fileScanned() for every scanned file.
   * \param filename
   */
    void scanFiles(QStringList fileNames);

signals:
    /*!
     * \brief fileScanned
     * Signal is emited when file processing is done.
     * \param filename
     */
    void filesScanned(QString csv);

private:
    class Impl;
    QScopedPointer<Impl> _impl;
};

} // namespace ct

#endif // CT_FILE_PROCESSOR_H
