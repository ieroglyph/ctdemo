#ifndef FPS_FRAMESDATA_CORE_EXPORT_H
#define FPS_FRAMESDATA_CORE_EXPORT_H

#ifdef _WIN32
#if defined FPS_FRAMESDATA_CORE_DLL
#define FPS_FRAMESDATA_CORE_EXPORT __declspec(dllexport)
#else
#define FPS_FRAMESDATA_CORE_EXPORT __declspec(dllimport)
#endif
#else
#define FPS_FRAMESDATA_CORE_EXPORT
#endif

#endif // FPS_FRAMESDATA_CORE_EXPORT_H
