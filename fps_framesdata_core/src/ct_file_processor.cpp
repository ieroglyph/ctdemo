#include "ct_file_processor.h"
#include "ct_file_processor_worker.h"

#include "impl.h"

#include <QByteArray>
#include <QDebug>
#include <QFile>
#include <QThreadPool>

namespace ct
{
CTFileProcessor::CTFileProcessor(QObject* parent) : QObject(parent), _impl(new Impl(this))
{
    QObject::connect(_impl.data(), &CTFileProcessor::Impl::filesScanned, this,
                     &CTFileProcessor::filesScanned);
}

CTFileProcessor::~CTFileProcessor()
{
}

void CTFileProcessor::scanFiles(QStringList fileNames)
{
    _impl->scanFiles(fileNames);
}

} // namespace ct
