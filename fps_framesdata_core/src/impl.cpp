#include "src/impl.h"
#include <QTextStream>

namespace ct
{
ct::CTFileProcessor::Impl::Impl(ct::CTFileProcessor* parent) : scanPool(parent)
{
}

ct::CTFileProcessor::Impl::~Impl()
{
}

void ct::CTFileProcessor::Impl::onFileScanned(QString fileName, int fileLength,
                                              ct::CTFileProcessor::DeviceMap map)
{
    if (fileLength > 0)
    {
        for (DeviceMap::iterator entry = map.begin(); entry != map.end(); ++entry)
        {
            deviceMap[entry.key()] += entry.value();
        }
        totalTime += fileLength;
    }
    filesProcessed++;

    if (filesProcessed == filesNum)
    {
        QString csv;
        QTextStream str(&csv);
        str.setRealNumberPrecision(5);
        for (auto it = deviceMap.constBegin(); it != deviceMap.constEnd(); ++it)
        {
            double value = static_cast<double>(it.value()) * 1000. / static_cast<double>(totalTime);
            str << it.key() << ',' << value << '\n';
        }
        emit filesScanned(csv);
    }
}

void CTFileProcessor::Impl::scanFiles(QStringList fileNames)
{
    scanPool.waitForDone();
    filesNum = fileNames.size();
    filesProcessed = 0;
    totalTime = 0;
    deviceMap.clear();
    for (const auto& filename : fileNames)
    {
        // QCTFileProcessorWorker::audoDelete returns true, so worker will be deleted by a thread pool automaticaly right after it's execution ends.
        // And by Qt::QueuedConnection we ensure that no race condition will take place.
        QCTFileProcessorWorker* worker = new QCTFileProcessorWorker(filename);
        worker->setAutoDelete(true);
        QObject::connect(worker, &QCTFileProcessorWorker::fileScanned, this,
                         &CTFileProcessor::Impl::onFileScanned, Qt::QueuedConnection);
        scanPool.start(worker);
    }
}

} // namespace ct
