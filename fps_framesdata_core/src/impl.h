#ifndef IMPL_H
#define IMPL_H

#include "ct_file_processor.h"
#include "ct_file_processor_worker.h"
#include <QObject>
#include <QStringList>
#include <QThreadPool>
#include <opencv2/opencv.hpp>

namespace ct
{
class CTFileProcessor::Impl : public QObject
{
    Q_OBJECT

public:
    explicit Impl(CTFileProcessor* parent);
    ~Impl();
    void onFileScanned(QString fileName, int fileLength, DeviceMap map);

    void scanFiles(QStringList fileNames);

signals:
    void filesScanned(QString map);

private:
    DeviceMap deviceMap;
    QThreadPool scanPool;
    int totalTime{ 0 };
    int filesNum{ 0 };
    int filesProcessed{ 0 };
};

} // namespace ct

#endif // IMPL_H
