#ifndef CT_FILE_PROCESSOR_WORKER_H
#define CT_FILE_PROCESSOR_WORKER_H

#include <QMap>
#include <QObject>
#include <QRunnable>
#include <QString>

namespace ct
{
class QCTFileProcessorWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    typedef QMap<QString, int> DeviceMap;

    QCTFileProcessorWorker(QString filename, QObject* parent = nullptr);
    void run() override;

signals:
    void fileScanned(QString filename, int fileLength, DeviceMap map);

private:
    QString _filename;
};

} // namespace ct
#endif // CT_FILE_PROCESSOR_WORKER_H
