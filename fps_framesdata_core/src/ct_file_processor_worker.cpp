#include "ct_file_processor_worker.h"

#include <map>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <QStringList>
#include <QThread>

namespace
{
constexpr char const* header{ "header" };
constexpr char const* captures{ "captures" };
constexpr char const* name{ "name" };
constexpr char const* shots{ "shots" };
constexpr char const* grabMsec{ "grabMsec" };
} // namespace

namespace ct
{
QCTFileProcessorWorker::QCTFileProcessorWorker(QString filename, QObject* parent) :
    QObject(parent), QRunnable(), _filename(filename)
{
}

void QCTFileProcessorWorker::run()
{
    try
    {
        cv::FileStorage fs(qPrintable(_filename), cv::FileStorage::READ);
        QStringList devices;
        cv::FileNode nodeDevices = fs[::header][::captures];
        for (auto device : nodeDevices)
        {
            devices << QString(device[::name].string().c_str());
        }

        DeviceMap deviceMap;

        cv::FileNode shots = fs[::shots];
        for (auto shot : shots)
        {
            for (auto device : devices)
            {
                if (!shot[qPrintable(device)].empty())
                    deviceMap[device]++;
            }
        }
        int fileLength = static_cast<int>(shots[static_cast<int>(shots.size() - 1)][::grabMsec]) -
                         static_cast<int>(shots[0][::grabMsec]);
        emit fileScanned(_filename, fileLength, deviceMap);
    }
    catch (cv::Exception& e)
    {
        std::cout << qPrintable(_filename) << ": " << e.what() << std::endl;
        emit fileScanned(_filename, 0, DeviceMap());
    }
}
} // namespace ct
