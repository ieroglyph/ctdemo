#include "include/main_window.h"
#include "ui_main_window.h"

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QString>
#include <QStringList>

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    processor = new ct::CTFileProcessor();
    processor->moveToThread(&thread);
    thread.start();
    QObject::connect(this, &MainWindow::sendSelectedFileNames, processor,
                     &ct::CTFileProcessor::scanFiles, Qt::QueuedConnection);
    QObject::connect(processor, &ct::CTFileProcessor::filesScanned, this, &MainWindow::onCsvCounted,
                     Qt::QueuedConnection);
}

MainWindow::~MainWindow()
{
    thread.quit();
    thread.wait();
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this);
    QDir dir(directory);

    QDirIterator it(dir.absolutePath(), QStringList() << "*.info.yml.gz", QDir::Files);

    ui->listFiles->blockSignals(true);

    while (it.hasNext())
    {
        it.next();
        ui->listFiles->addItem(it.fileName());
    }

    ui->listFiles->sortItems();

    ui->listFiles->blockSignals(false);

    ui->label->setText(dir.absolutePath());
}

void MainWindow::on_listFiles_itemSelectionChanged()
{
    QStringList files;
    for (const auto& item : ui->listFiles->selectedItems())
    {
        QDir dir(ui->label->text());
        files << dir.filePath(item->text());
    }
    emit sendSelectedFileNames(files);
}

void MainWindow::onCsvCounted(const QString& csv)
{
    ui->textCsv->setText(csv);
}
