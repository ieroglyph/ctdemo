#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QThread>

#include "ct_file_processor.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_listFiles_itemSelectionChanged();

    void onCsvCounted(const QString& csv);

signals:

    void sendSelectedFileNames(QStringList fileNames);

private:
    Ui::MainWindow* ui;
    ct::CTFileProcessor* processor;
    QThread thread;
};

#endif // MAIN_WINDOW_H
