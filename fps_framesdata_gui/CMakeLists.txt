cmake_minimum_required(VERSION 3.4)

project(fps_framesdata_gui LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 11)

add_executable(${PROJECT_NAME} "")

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)

find_package(Qt5 ${QT_REQUIRED_VERSION} CONFIG REQUIRED COMPONENTS Widgets Core)

file(GLOB_RECURSE SOURCES "src/*" "include/*")
target_sources(${PROJECT_NAME} PRIVATE ${SOURCES})

target_include_directories(${PROJECT_NAME} PUBLIC ${INCLUDES})
include_directories(include)

target_link_libraries(${PROJECT_NAME} PUBLIC fps_framesdata_core Qt5::Widgets Qt5::Core)
