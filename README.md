# ctdemo

## Description

Test task for C.* T.*

This project consists of three targets:

1. fps_framesdata_core

Provides some classes that implement basic work with *.info.yml.gz files, given fields, fps calculation.

2. fps_framesdata

Basic CLI application that works with command line params

3. fps_framesdata_gui

Additional GUI application.

## Algoritm
The implemented FPS Calculation algorithm:
1. For each file:
   1. Calc duration $`\Delta t_{dev}`$, represented by this file;
   2. Count how many times each device appeared in this file, $`n_{dev}`$;
2. Accumulate the total duration $`\Delta T_{dev}`$ of records and total ammount $`N_{dev}`$ of appearences of each device;
3. For each device calculate the FPS as follows:

```math
FPS_{dev} = \Delta T_{dev} / N_{dev}
```

The algorithm can be improved by combining records of consequent files in one singe episode.

## Requirements:

1. CMake version 3.2 or above

2. Compiler supporting c++11

3. For all targets: OpenCV 3.2, Qt 5.10
